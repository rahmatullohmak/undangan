-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Jun 2023 pada 07.48
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maknende_semester`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_undangan`
--

CREATE TABLE `data_undangan` (
  `mp` varchar(50) NOT NULL,
  `ml` varchar(50) NOT NULL,
  `hari` varchar(50) NOT NULL,
  `bulan` varchar(50) NOT NULL,
  `tgl` varchar(50) NOT NULL,
  `tahun` varchar(50) NOT NULL,
  `status_anakl` varchar(50) NOT NULL,
  `status_anakp` varchar(50) NOT NULL,
  `foto_priwed` varchar(50) NOT NULL,
  `jam_akad` varchar(50) NOT NULL,
  `lokasi_akad` varchar(50) NOT NULL,
  `jam_repsesi` varchar(50) NOT NULL,
  `lokasi_resepsi` varchar(50) NOT NULL,
  `jadwal_akad` varchar(50) NOT NULL,
  `jadwal_repsesi` varchar(50) NOT NULL,
  `id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_undangan`
--

INSERT INTO `data_undangan` (`mp`, `ml`, `hari`, `bulan`, `tgl`, `tahun`, `status_anakl`, `status_anakp`, `foto_priwed`, `jam_akad`, `lokasi_akad`, `jam_repsesi`, `lokasi_resepsi`, `jadwal_akad`, `jadwal_repsesi`, `id`) VALUES
('aku', 'dia', 'minggu', 'agustus', '20', '2024', ' laki laki dari bapaknya', 'perempuan dari ibunya', 'prewed2.png', '12:00-selesai', 'rumahnya', '14:00-selesai', 'rumah', 'agustus', 'senin,12 agustus 2024', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gbr`
--

CREATE TABLE `gbr` (
  `id` int(255) NOT NULL,
  `gbr1` varchar(255) NOT NULL,
  `gbr2` varchar(255) NOT NULL,
  `gbr3` varchar(255) NOT NULL,
  `gbr4` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gbr`
--

INSERT INTO `gbr` (`id`, `gbr1`, `gbr2`, `gbr3`, `gbr4`) VALUES
(1, '108167_pernikahan-ben-kasyafani-marshanda.jpg', 'Gambar2.jpg', 'IMG_0820.jpg', 'kian.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `id` int(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`id`, `nama`, `password`) VALUES
(36, 'ooo', 'ooo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `undangan`
--

CREATE TABLE `undangan` (
  `id` int(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `komen` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `undangan`
--

INSERT INTO `undangan` (`id`, `nama`, `komen`) VALUES
(58, 'laila', 'hadir'),
(66, 'Para Tamu Undangan', 'hadir'),
(67, 'Para Tamu Undangan', 'hadir'),
(68, 'Para Tamu Undangan', 'hadir'),
(69, 'Para Tamu Undangan', 'hadir'),
(70, 'rahmat', 'hadir');

-- --------------------------------------------------------

--
-- Struktur dari tabel `undangann`
--

CREATE TABLE `undangann` (
  `id` int(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `komen` varchar(255) NOT NULL,
  `TANGGAL` varchar(23) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `undangann`
--

INSERT INTO `undangann` (`id`, `nama`, `komen`, `TANGGAL`) VALUES
(26, 'laila', 'rgrgr', 'March 15, 2023, 4:00 am'),
(63, 'rahmat', 'good', 'March 17, 2023'),
(64, 'aryyan dolu', 'goddd', 'March 17, 2023'),
(65, 'Para Tamu Undangan', 'gagahhhhhhhhhhhhhhhhhh', 'April 26, 2023'),
(66, 'Para Tamu Undangan', 'uuuuuuuuuuuuu', 'April 26, 2023'),
(73, 'rahmat', 'godddddd', 'June 9, 2023');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `user`, `password`) VALUES
(1, 'rahmat', 'qqqq');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_undangan`
--
ALTER TABLE `data_undangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gbr`
--
ALTER TABLE `gbr`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `undangan`
--
ALTER TABLE `undangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `undangann`
--
ALTER TABLE `undangann`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_undangan`
--
ALTER TABLE `data_undangan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `gbr`
--
ALTER TABLE `gbr`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `login`
--
ALTER TABLE `login`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `undangan`
--
ALTER TABLE `undangan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT untuk tabel `undangann`
--
ALTER TABLE `undangann`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
