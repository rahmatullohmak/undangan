<!DOCTYPE html>
<html>
<head>
  <title>Contoh Formulir dengan Database</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #f4f4f4;
      padding: 20px;
    }
    
    .container {
      max-width: 500px;
      margin: 0 auto;
      background-color: #fff;
      padding: 20px;
      border-radius: 5px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }
    
    .form-group {
      margin-bottom: 20px;
    }
    
    .form-group label {
      display: block;
      font-weight: bold;
      margin-bottom: 5px;
    }
    
    .form-group input[type="text"],
    .form-group input[type="email"],
    .form-group textarea {
      width: 100%;
      padding: 10px;
      border: 1px solid #ccc;
      border-radius: 3px;
    }
    
    .form-group button {
      padding: 10px 20px;
      background-color: #4CAF50;
      color: #fff;
      border: none;
      border-radius: 3px;
      cursor: pointer;
    }
    
    .form-group button:hover {
      background-color: #45a049;
    }
    
    table {
      width: 100%;
      border-collapse: collapse;
      margin-top: 20px;
    }
    
    table th, table td {
      padding: 8px;
      text-align: left;
      border-bottom: 1px solid #ddd;
    }
    
    table th {
      background-color: #f2f2f2;
    }
  </style>
</head>
<body>
  <div class="container">
    <h2>Contoh Formulir</h2>
    <form action="index.php" method="POST">
      <div class="form-group">
        <label for="nama">Nama:</label>
        <input type="text" id="nama" name="nama" required>
      </div>
      <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required>
      </div>
      <div class="form-group">
        <label for="pesan">Pesan:</label>
        <textarea id="pesan" name="pesan" rows="4" required></textarea>
      </div>
      <div class="form-group">
        <button type="submit">Kirim</button>
      </div>
    </form>

    <?php
    // Konfigurasi koneksi database
    $host = "localhost";
    $username = "root";
    $password = "";
    $database = "tess";

    // Menghubungkan ke database
    $conn = mysqli_connect($host, $username, $password, $database);

    // Memeriksa koneksi
    if (!$conn) {
        die("Koneksi database gagal: " . mysqli_connect_error());
    }

    // Memproses data formulir
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nama = $_POST["nama"];
        $email = $_POST["email"];
        $pesan = $_POST["pesan"];

        // Menyimpan data formulir ke dalam database
        $sql = "INSERT INTO formulir (nama, email, pesan) VALUES ('$nama', '$email', '$pesan')";
        if (mysqli_query($conn, $sql)) {
            echo "Data formulir berhasil disimpan ke database.";
        } else {
            echo "Gagal menyimpan data formulir ke database: " . mysqli_error($conn);
        }
    }

    // Menutup koneksi database
    mysqli_close($conn);
    ?>

    <?php
    // Menampilkan data formulir dari database dalam tabel
    $conn = mysqli_connect($host, $username, $password, $database);
    if (!$conn) {
        die("Koneksi database gagal: " . mysqli_connect_error());
    }

    $sql = "SELECT * FROM formulir";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        echo '<h2>Data Formulir</h2>';
        echo '<table>';
        echo '<tr><th>Nama</th><th>Email</th><th>Pesan</th></tr>';
        
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>';
            echo '<td>'.$row["nama"].'</td>';
            echo '<td>'.$row["email"].'</td>';
            echo '<td>'.$row["pesan"].'</td>';
            echo '</tr>';
        }
        
        echo '</table>';
    }

    mysqli_close($conn);
    ?>
  </div>
</body>
</html>