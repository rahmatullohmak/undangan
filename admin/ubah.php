<?php


if(!isset($_SESSION["loginn"])){
    header("Location:log.php");
    die();

}

include 'conn.php';
// Retrieve the data from the database for pre-filling the form
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Prepare the SQL statement to fetch the data from the table
    $sql = "SELECT * FROM undangann WHERE id = '$id'";

    // Execute the SQL statement
    $result = $conn->query($sql);

    // Check if there is a row returned
    if ($result->num_rows > 0) {
        // Fetch the data from the row
        $row = $result->fetch_assoc();
        $nama = $row['nama'];
        $komen = $row['komen'];
        $tanggal = $row['TANGGAL'];
    } else {
        // No row found, redirect to another page or display an error message
        echo "No data found.";
        exit;
    }
} else {
    // ID parameter is missing, redirect to another page or display an error message
    echo "Invalid ID parameter.";
    exit;
}

// Check if the form is submitted
if(isset($_POST['submit'])) {
    // Retrieve the form data
    $nama = $_POST['nama'];
    $komen = $_POST['komen'];
    $tanggal = $_POST['tanggal'];

    // Prepare the SQL statement to update the data in the table
    $sql = "UPDATE undangann SET nama = '$nama', komen = '$komen', TANGGAL = '$tanggal' WHERE id = '$id'";

    // Execute the SQL statement
    if ($conn->query($sql) === TRUE) {
      
echo"

<script>

document.location.href='index.php';


</script>";
    } else {
        echo "Error updating data: " . $conn->error;
    }
}

// Close the database connection
$conn->close();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Ubah Data</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            padding: 20px;
        }

        h2 {
            margin-bottom: 20px;
        }

        form {
            background-color: #ffffff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
        }

        .form__field {
            margin-bottom: 20px;
        }

        .form__field label {
            display: block;
            font-weight: bold;
            margin-bottom: 5px;
        }

        .form__field input {
            width: 100%;
            padding: 10px;
            border: 1px solid #cccccc;
            border-radius: 3px;
        }

        .form__submit {
            text-align: right;
        }

        .form__submit input {
            background-color: #4caf50;
            color: #ffffff;
            border: none;
            padding: 10px 20px;
            border-radius: 3px;
            cursor: pointer;
        }

        .form__submit input:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <h2>Ubah Data</h2>
    <form action="" method="POST">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="form__field">
            <label for="nama">Nama:</label>
            <input type="text" name="nama" value="<?php echo $nama; ?>">
        </div>
        <div class="form__field">
            <label for="komen">Komentar:</label>
            <input type="text" name="komen" value="<?php echo $komen; ?>">
        </div>
        <div class="form__field">
            <label for="tanggal">Tanggal:</label>
            <input type="text" name="tanggal" value="<?php echo $tanggal; ?>">
        </div>
        <div class="form__submit">
            <input type="submit" name="submit" value="Simpan">
        </div>
    </form>
</body>
</html>